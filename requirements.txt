gdal>=2.3.3
h5py>=2.9.0
matplotlib>=3.3.2
numpy>=1.17.2
numba>=0.48.0
scikit-learn>=0.21.3
tqdm>=4.42.0